'''
author: jakeret
'''


from __future__ import print_function, division, absolute_import, unicode_literals
import cv2
import glob
import numpy as np
from PIL import Image # Python Imaging Library 
import matplotlib.pyplot as plt

class ImageDataProvider(object):    

    def __init__(self, search_path, data_suffix=".png", shuffle_data=False ):

        self.data_suffix = data_suffix
        self.file_idx = -1
        self.shuffle_data = shuffle_data
        self.data_files = self._find_data_files(search_path)
        
        assert len(self.data_files) > 0, "No training files"
    
        
        img = self._load_file(self.data_files[0])
        self.channels = 1 if len(img.shape) == 2 else img.shape[-1]
        
    def _load_data(self):
    
        train_data = self._next_data()
        nx = train_data.shape[1]
        ny = train_data.shape[0]
        return train_data.reshape(1, ny, nx, self.channels)


    def __call__(self, n):
        train_data = self._load_data()
        nx = train_data.shape[1]
        ny = train_data.shape[2]
    
        X = np.zeros((n, nx, ny, self.channels))
         
        X[0] = train_data
      
        for i in range(1, n):
            train_data = self._load_data()
            X[i] = train_data
        
    
        return X



    def _find_data_files(self, search_path):

        all_files_aux = glob.glob(search_path)
        all_files = sorted (all_files_aux)
        
        return [name for name in all_files if self.data_suffix in name]
    
    
    def _load_file(self, path, dtype=np.float32):
        return np.array(Image.open(path), dtype)
        

    def _cylce_file(self):
        self.file_idx += 1
        if self.file_idx >= len(self.data_files):
            self.file_idx = 0 
            if self.shuffle_data:
                np.random.shuffle(self.data_files)
        
    def _next_data(self):
        self._cylce_file()
    
        image_name = self.data_files[self.file_idx]
        #print('image name:  ',image_name)
    
        img = self._load_file(image_name, np.float32)

        return img
