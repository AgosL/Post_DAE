# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 10:01:42 2019
@author: alarrazabal
"""

import os
import image_util
import numpy as np
import cv2
import skimage.io as io
import skimage.transform as trans
from keras.layers import*
import Post_DAE_model as Post_DAE
from keras.utils import np_utils
import keras
from keras.models import load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint,TensorBoard
from keras.optimizers import Adam
from time import time
import matplotlib.pyplot as plt


def ensure_dir(directory):
  if not os.path.exists(directory):
      os.makedirs(directory)
      
smooth = 1.
def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

 
def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred) 

  
def drawLines(x,swap_prob=0.6):
 
    batch_size, height, width = x.shape[:-1]
    noised=x.copy()  

    for i in range(batch_size):
        

            numLines = np.random.randint(1,100)  
            for l in range(numLines):
                   cv2.circle(noised[i,:,:], (np.random.randint(100,950),np.random.randint(100,950)), np.random.randint(60), (1,1,1), -1)
     
            k=np.random.randint(10,30) 
            kernel = np.ones((k,k),np.uint8)
            noised[i,:,:,0] =  cv2.morphologyEx(noised[i,:,:], cv2.MORPH_CLOSE, kernel) 
            
            numLines = np.random.randint(1,80) 
            for l in range(numLines):
                  cv2.circle(noised[i,:,:], (np.random.randint(150,900),np.random.randint(150,900)), np.random.randint(30), (0, 0, 0), -1)

            if np.random.uniform(low=0.0,high=1.0) > 0.3:
                
                swap_map = np.random.choice(range(2), size=(1024, 1000 / 15), p=[1 - swap_prob, swap_prob])
                h_idx, w_idx = np.where(swap_map == 1)
                w_idx = 15* (w_idx + 1)
                bor=15            
                for b in range(bor):
                    x_r_vals = noised[i, h_idx, w_idx+b, 0]
                    x_l_vals = noised[i, h_idx,w_idx-(1+b) , 0]
                    noised[i, h_idx, w_idx+b, 0] = x_l_vals
                    noised[i, h_idx, w_idx-(1+b) , 0] = x_r_vals
            
            if np.random.uniform(low=0.0,high=1.0) > 0.4:       
                kernel = np.ones((1,7),np.uint8)
                noised[i,:,:,0] =  cv2.morphologyEx(noised[i,:,:], cv2.MORPH_CLOSE, kernel) 

         
    return noised       
def generate_validation_data(VAL_IMAGE_DIR,nval):
    data_provider_val = image_util.ImageDataProvider(VAL_IMAGE_DIR)
    x_val=data_provider_val(nval)
    x_val_categorical= np_utils.to_categorical(x_val, 3)
  
    # Channel 0 - Background
    # Channel 1 - Heart
    # Channel 2 - Lung
    
    x_val_noise = np.copy(x_val_categorical)
    x_val_Lnoise = drawLines(x_val_noise[:,:,:,2].reshape(len(x_val_noise),1024,1024,1),0.2,part='lung')
    x_val_Hnoise = drawLines(x_val_noise[:,:,:,1].reshape(len(x_val_noise),1024,1024,1),0.2,part='heart')
    x_val_noise=np.zeros((nval,1024,1024,3))
    
    for i in range(nval):
        x_val_noise[i]=mix_noise(x_val_Lnoise[[i]],x_val_Hnoise[[i]],channels=3)       
        
    return x_val_noise , x_val_categorical        
   
def generate_validation_data(VAL_IMAGE_DIR,nval):

    data_provider = image_util.ImageDataProvider(search_path=VAL_IMAGE_DIR)
    x_val= data_provider(nval)  
    x_val_noise = np.copy(x_val)
    x_val_noise = drawLines(x_val_noise)
    
    return x_val_noise , x_val  
    
def save_image_with_scale(path, arr, binary = True):
    if binary:
        arr[arr >= 0.5] = 1
        arr[arr < 0.5] = 0
        
    arr = arr * 255.
    arr = arr.astype(np.uint8)
    io.imsave(path, arr)    
    
def saveResult(save_path,npyfile, filenames = None,ep =None):
    for i,item in enumerate(npyfile):
        img = item[:,:,0]
        if filenames is not None:
            save_image_with_scale(os.path.join(save_path,filenames[i]),img, binary=True)
        else:
            save_image_with_scale(os.path.join(save_path,"%d_predict.png"%i),img)    
    
    
def testGenerator(test_path, listOfFilenames, target_size = (1024, 1024), as_gray = True):
    
    for root, directories, filenames in os.walk(test_path):
        #print('Filenames: ',filenames)
        for filename in filenames:
            if filename.endswith(".png"):
                #print("Processing " + filename)
                listOfFilenames.append(filename)
                imgaux = io.imread(os.path.join(test_path,filename),as_gray = as_gray)
                img = np.zeros(shape = imgaux.shape, dtype = np.float)
                img[imgaux > 0.5] = 1
                img = img[:,:]
                img = trans.resize(img,target_size)
                img = np.reshape(img,img.shape+(1,))
                img = np.reshape(img,(1,)+img.shape)

                yield img
                                

def save_PostDAE_RF(TEST_IMAGE_DIR,TEST_IMAGE_SAVE_DIR, saveDir,ntest=None):   
   
    w_name='/AutoEncoder_w.hdf5'   
    
    model=Post_DAE.binary() 
    model.load_weights(saveDir + w_name)
    
    listOfFilenames=[]
    test_seg = testGenerator(TEST_IMAGE_DIR, listOfFilenames)
  
        
    results = model.predict_generator(test_seg,ntest,verbose=1)
    ensure_dir(TEST_IMAGE_SAVE_DIR)
    saveResult(TEST_IMAGE_SAVE_DIR,results, listOfFilenames)# 
  
def save_PostDAE_UNet(TEST_IMAGE_DIR,TEST_IMAGE_SAVE_DIR, saveDir,ntest=51,MC=False):

    w_name='/AutoEncoder_w.hdf5'
    if MC==False:
        model=Post_DAE.binary() 
    if MC==True:
        model=Post_DAE.multiclass() 
        
    model.load_weights(saveDir + w_name)
    
    for e in range(5,61,5):
    
        TEST_IMAGE_DIR_EPOCH = TEST_IMAGE_DIR +str(e)+"_ep/"
        
        listOfFilenames=[]
        if MC == False:
            test_seg = testGenerator(TEST_IMAGE_DIR_EPOCH, listOfFilenames)
        if MC == True:
            test_seg = testGeneratorMC(TEST_IMAGE_DIR_EPOCH, listOfFilenames)
                    
        results = model.predict_generator(test_seg,ntest,verbose=1)
        ensure_dir(TEST_IMAGE_SAVE_DIR+str(e)+"_ep/")
        if MC == False:
            saveResult(TEST_IMAGE_SAVE_DIR+str(e)+"_ep/",results, listOfFilenames)   
        if MC == True:
            saveResultMC(TEST_IMAGE_SAVE_DIR+str(e)+"_ep/",results, listOfFilenames)    

      
def generator(features=None, batch_size=None):

     batch_features = np.zeros((batch_size, 1024,1024,1))
     batch_labels = np.zeros((batch_size, 1024,1024,1))
    
     while True:
        
       for i in range(batch_size):
        
         index= np.random.choice(len(features),1) 
         batch_labels[i]=features[index]

         if np.random.uniform(low=0.0,high=1.0)<0.9:
             batch_features[i]  = drawLines(features[index].reshape(1,1024,1024,1))
         else:
             batch_features[i] ==features[index]
             
         if (np.random.uniform(low=0.0,high=1.0)<0.7):
             fx=np.random.uniform(low=0.8,high=1.0)
             fy=np.random.uniform(low=0.8,high=1.0)                

             lab = cv2.resize(batch_labels[i], (0,0), fx=fx, fy=fy) 
             feat = cv2.resize(batch_features[i], (0,0), fx=fx, fy=fy)
              
             batch_labels[i]= batch_labels[i]*0
             batch_features[i]= batch_features[i] *0
             h,w=np.shape(lab)

             if h == 1024:
                ho=0
             else:   
                ho=np.random.randint(0,(1024-h))
             if w== 1024:
                wo=0
             else:   
                wo=np.random.randint(0,(1024-w))
             batch_features[i,ho:(ho+h),wo:(wo+w),0]= feat
             batch_labels[i,ho:(ho+h),wo:(wo+w),0]= lab

       yield batch_features, batch_labels 
      


def train_new_model(TRAIN_IMAGE_DIR,saveDir,x_val,y_val, ntrain,bz, epochs,use_pretrained_w=False, lr=0.001,MC=False):

        data_provider = image_util.ImageDataProvider(TRAIN_IMAGE_DIR)
        x_train = data_provider(ntrain)
        if MC==False:
            model=Post_DAE.binary()
        if MC==True:
            model=Post_DAE.multiclass()
        model.compile(optimizer=Adam(lr=lr), loss=dice_coef_loss, metrics = [dice_coef])

        tensorboard = TensorBoard(log_dir=saveDir+'/logs'.format(time()))
        w_name='/AutoEncoder_w.hdf5'
        
        if use_pretrained_w ==True:    
            model.load_weights(saveDir + w_name)

        chkpt = saveDir + w_name
        cp_cb = ModelCheckpoint(filepath = chkpt, monitor='val_loss', verbose=1, save_best_only=False, mode='auto')
        
        history = model.fit_generator(generator(x_train, batch_size=bz),
                              epochs=epochs,
                              verbose=1,
                              validation_data=(x_val, y_val),samples_per_epoch=(ntrain),
                              callbacks=[cp_cb,tensorboard],
                              shuffle=False) 
                
                

def show_comparison(orig, noise, denoise, num=5):
    
    index=range(len(orig))
    np.random.shuffle(index)
    
    n = num
    plt.figure(figsize=(16, 10))

    for i in range(n):
        # display original
        ax = plt.subplot(3, n, i+1)
        plt.imshow(orig[index[i]].reshape(1024,1024))
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display noisy image
        ax = plt.subplot(3, n, i +1 + n)
        plt.imshow(noise[index[i]].reshape(1024,1024))
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        
        # display denoised image
        ax = plt.subplot(3, n, i +1 + n + n)
        plt.imshow(denoise[index[i]].reshape(1024,1024))
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()
    plt.savefig('comparison.png')
                    
