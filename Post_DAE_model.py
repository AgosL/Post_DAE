
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 14:14:30 2018
@author: alarrazabal
"""
from keras.models import Sequential
from keras.optimizers import Adam
from keras.models import Model
from keras.layers import*
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, BatchNormalization, Activation, Flatten, Reshape



def binary(pretrained_weights = None,inp_s=(1024, 1024,1)): 

    input_img = Input(shape=inp_s)
    x = Conv2D(16, 3,strides=2, padding='same')(input_img)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(32, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(32, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(32, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)

    
    x = Flatten()(x)
    x = Dense(512)(x)
    x = Dense(1024)(x)
    x = Activation('relu')(x)
    
    
    encoded=Reshape((32,32,1))(x)

    encoded= UpSampling2D(size=(2,2))(encoded)
    x = Conv2D(16, 3,strides= 1, padding='same')(encoded)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    
    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    
    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    
    
    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    
    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(1, 3,strides= 1, padding='same')(x)
    decoded = Activation('sigmoid')(x)
   
    model = Model(input_img, decoded)

    return model



def multiclass(pretrained_weights = None,inp_s=(1024, 1024,3)): 

    input_img = Input(shape=inp_s)
    x = Conv2D(16, 3,strides=2, padding='same')(input_img)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(32, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(32, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(32, 3,strides=1, padding='same')(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 3,strides=2, padding='same')(x)
    x = Activation('relu')(x)

    x = Flatten()(x)
    x = Dense(1024)(x)
    x = Dense(4096)(x)
    x = Activation('relu')(x)

   
    encoded=Reshape((64,64,1))(x)

    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    
    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    
    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    
    
    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    

    x= UpSampling2D(size=(2,2))(x)
    x = Conv2D(16, 3,strides= 1, padding='same')(x)
    x = Activation('relu')(x)
    x = Conv2D(3, 3,strides= 1, padding='same')(x)
    decoded = Activation('softmax')(x)
    
    model = Model(input_img, decoded)

    return model
